# Full Site Editing

Full Site Editing (FSE) is an umbrella project which makes possible to edit full WOrdPress website and not only posts and pages. These (major) projects are:

- **Site Editor**: the cohesive experience that allows you to directly edit and navigate between various templates, template parts, styling options, and more.
- **Template Editing**: a scaled down direct editing experience allowing you to edit/change/create the template a post/page uses.
- **Block Theme**: work to allow for a theme that’s built using templates composed using blocks that works with full site editing. More below.
- **Styling**: the feature that enables styling modifications across three levels (local blocks, theme defaults, and global modifications).
- **Theme Blocks**: new blocks that accomplish everything possible in traditional templates using template tags (ex: Post Author Block).
- **Browsing**: the feature that unlocks the ability to navigate between various entities in the site editing experience including templates, pages, etc.
- **Navigation Block**: a new block that allows you to edit a site’s navigation menu, both in terms of structure and design.
- **Query Block**: a new block that replicates the classic WP_Query and allows for further customization with additional functionality.

As a starter setup a test site with the Gutenberg plugin and a block theme. [TT1 Blocks](https://wordpress.org/themes/tt1-blocks/) theme is recommended.

## Get Involved

An [FSE Outreach Proram](https://make.wordpress.org/test/handbook/full-site-editing-outreach-experiment/) has instructions on [How to Test FSE](https://make.wordpress.org/test/handbook/full-site-editing-outreach-experiment/how-to-test-fse/).

## Block Themes

See the [block theme how-to](https://developer.wordpress.org/block-editor/how-to-guides/themes/) for detailed overview and walk-through.

See the [WordPress/theme-experiments](https://github.com/WordPress/theme-experiments/) repository for examples.

Use the `new-empty-theme.php` script from theme-experiments repo to generate a starter theme:

```shell
❯ git clone https://github.com/WordPress/theme-experiments
❯ cd theme-experiments
❯ php new-empty-theme.php
Please provide the following information:
Theme name: TestTheme
Description: A theme to test
Author: <your-name>
Theme URI: https://github.com/<your-theme-uri>
 
 Your new theme is ready!
```

Copy generated directory to your `wp-content/themes` and start playing with your ideas.

See the [architecture document on templates](https://developer.wordpress.org/block-editor/explanations/architecture/full-site-editing-templates/) in the FSE.

See the [`theme.json` documentation](https://developer.wordpress.org/block-editor/how-to-guides/themes/theme-json/) on how to define theme settings.
