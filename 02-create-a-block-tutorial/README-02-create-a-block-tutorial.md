# Create a Block Tutorial

Creating a block for the WordPress Block Editor ([link](https://developer.wordpress.org/block-editor/getting-started/create-block/)).

## WordPress Plugin

1. Switch to Working Directory

Change to your local WordPress plugin directory, which should be `wp-content/plugins` (or `wp-content/mu-plugins` if you like) inside WordPress installation path.
If using `wp-env start`, you can start from within any directory.

2. Generate Plugin Files

Scaffold plugin files with:

```shell
npx @wordpress/create-block <plugin-name>
cd <plugin-name>
```

3. Start WordPress

Run your local environment or start from within plugin directory:

```shell
wp-env start
```

Login to the WordPress panel and confirm that the <plugin-name> plugin is installed and activated.

4. Edit, Build and Debug

Adjust your npm scripts in the `package.json` file. For editing plugin files run `npm run start` (or `npm start` for short). To build for production run `npm run build`.
Start testing, troubleshooting and debugging in the developer tools in your browser. Firefox provides [extensive documentation](https://firefox-source-docs.mozilla.org/devtools-user/index.html).

## Anatomy of the Block

Most of the properties are set in the `src/block.json` file. These properties are loaded by the main plugin's PHP file and `src/index.js` file.

`registerBlockType` function takes `name` and block object parameters. The `edit` and `save` properties are functions.

The results of the edit function is what the editor will render to the editor page when the block is inserted.

The results of the save function is what the editor will insert into the post_content field when the post is saved. The post_content field is the field in the WordPress database used to store the content of the post.

You can find block registration API reference [here](https://developer.wordpress.org/block-editor/reference-guides/block-api/block-registration/). Also JSON schema of the `block.json` file is set to https://schemas.wp.org/trunk/block.json.

Internationalization functions are imported in `src/save.js` and `src/edit.js` files.

## Block Attributes

Attributes are the way a block stores data, they define how a block is parsed to extract data from the saved content.

Attributes are stored in the `block.json` file.

You can find block attributes API reference [here](https://developer.wordpress.org/block-editor/reference-guides/block-api/block-attributes/).

The attributes are passed to the `edit` and `save` functions, along with a `setAttributes` function to set the values.

You can find save and edit reference [here](https://developer.wordpress.org/block-editor/reference-guides/block-api/block-edit-save/).

For IDE support (code completion and hints), you can install the `@wordpress/components` module which imports block components. This install is optional since the build process automatically detects `@wordpress/*` imports and specifies as dependencies in the assets file.

## Code Implementation

The enqueue process is run by the scripts in the main plugin's `.php` file. The `register_block_type` function checks the `build/block.json` file and enqueues JS and CSS files.

The `build/index.css` is compiled from `src/editor.scss` and loads only within the editor, and after the `style-index.css`.
The `build/style-index.css` is compiled from `src/style.scss` and loads in both the editor and front-end.

The block classname is prefixed with `wp-block`. The `create-block/gutenpride` is converted to the classname `.wp-block-create-block-gutenpride`.

## Authoring Experience

What you see in the editor should be as close as possible to what you get when published. Keep this in mind when building blocks.

There is a Placeholder component built that gives us a standard look. It is used mostly with the image and embed blocks. To use the Placeholder, wrap the `<TextControl>` component so it becomes a child element of the `<Placeholder>` component. The placeholder can be useful if you are replacing block after typing or inserting something.

This can be done with a ternary function which is:
```javascript
clause ? doIfTrue : doIfFalse;
```
We need to pair the clause of this function with additional `isSelected` parameter. It is passed in to the edit function and returns `true` if the block is selected. Without this parameter check input control may disappear as soon as we begin to type.

The switching between a placeholder and input control works well with a visual element like an image or video. For text input it is better to add proper styling of the editor text inside `src/editor.scss` file. It should fit front-end styling.

## Finishing Touches

Use some additional concepts and structure to create or upgrade your block.

Additional components are provided by the [components package](https://developer.wordpress.org/block-editor/reference-guide/components/). Also read the [reference](https://wordpress.github.io/gutenberg).

Additional tutorials and guides are [RichText component reference](https://developer.wordpress.org/block-editor/reference-guides/richtext/), [Block controls tutorial](https://developer.wordpress.org/block-editor/how-to-guides/block-tutorial/block-controls-toolbar-and-sidebar/), [Sidebar tutorial](https://developer.wordpress.org/block-editor/how-to-guides/sidebar-tutorial/plugin-sidebar-0/) and [InnerBlocks documentation](https://developer.wordpress.org/block-editor/how-to-guides/block-tutorial/nested-blocks-inner-blocks/). Use them to improve a text input and plugin controls.

Core block source which can be found in the [block library package](https://github.com/WordPress/gutenberg/tree/HEAD/packages/block-library/src). These may inspire some additional ideas for your blocks.

## Share your Block with the World

Step 1: Help users understand your block

*Guidelines*

1. Name your block based on what it does

Choose something that is easily to search by name.
e.g. `Gutenpride Fancy Font Text Box by WordPress Developer Handbook`

2. Clearly describe your block

Short, condensed, clear and "to the point" description.
e.g. `A text box with a fancy font.`

3. Add keywords for all context

Extra content for a block.
e.g. `gallery, slider, text field, fancy font`

4. Choose the right category

Indicate core or custom block category. Core categories are:
`text, media, design, widgets, theme, embed`

Step 2: Analyze your plugin

Add `name`, `description`, `keywords` and `category` to the `block.json` file of your block.

```JSON
{
    "name": "create-block/gutenpride",
    "title": "Gutenpride Fancy Font Text Box by WordPress Developer Handbook",
    "description": "A text box witha a fancy font.",
    "keywords": [ "text field", "fancy font" ],
    "category": "text"
}
```

Step 3: Zip & Submit

Read [the block submitting guidelines](https://github.com/WordPress/wporg-plugin-guidelines/blob/block-guidelines/blocks.md).
Go to [the block plugin validator](https://wordpress.org/plugins/developers/block-plugin-validator/).
Upload your plugin.
