import { registerBlockType } from '@wordpress/blocks';

registerBlockType( 'myguten/test-block', {
	title: 'Myguten Test Block',
	icon: 'smiley',
	category: 'design',
	edit: () => <div>Hello world!</div>,
	save: () => <div>Hello world!</div>,
} );
