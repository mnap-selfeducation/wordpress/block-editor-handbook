<?php
/*
 * Plugin Name: Myguten Test Block
 */

$asset_file = include( plugin_dir_path( __FILE__ ) . 'build/index.asset.php' );

function myguten_test_block_enqueue_scripts() {
	wp_enqueue_script(
		'myguten-test-block-script',
		plugins_url( 'build/index.js', __FILE__ ),
		$asset_file['dependencies'],
		$asset_file['version']
	);
}
add_action( 'enqueue_block_editor_assets', 'myguten_test_block_enqueue_scripts' );
