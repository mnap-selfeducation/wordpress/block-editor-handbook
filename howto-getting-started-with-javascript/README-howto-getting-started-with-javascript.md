# Getting Started with JavaScript

## Plugins Background

Start extending WordPress with a plugin. Create directory called `myguten-plugin` and add code in the `myguten-plugin.php` file:

```PHP
<?php
/*
 * Plugin Name: Fancy Quote
 */
```

Put your plugin into `wp-content/plugins/` directory.

Find more about plugins in the [Plugin Basics](https://developer.wordpress.org/plugins/plugin-basics/) documentation.

## Loading JavaScript

Use `enqueue_block_editor_assets` hook in `myguten-plugin.php` file to enqueue `myguten.js` JavaScript file:

```PHP
function myguten_enqueue_scripts() {
	wp_enqueue_script(
		'myguten-script',
		plugins_url( 'myguten.js', __FILE__ )
	);
}
add_action( 'enqueue_block_editor_assets', 'myguten_enqueue_scripts' );
```

```JavaScript
console.log( "I'm loaded!" );
```

Create a new post and you should see "I'm loaded!" text in the browser console.

If you are extending the block editor for your theme you should replace `plugins_url()` function with `get_template_directory_uri()`. So enqueue JavaScript block for themes is:

```PHP
function myguten_enqueue() {
    wp_enqueue_script(
        'myguten-script',
        get_template_directory_uri() . '/myguten.js'
    );
}
add_action( 'enqueue_block_editor_assets', 'myguten_enqueue' );
```

## Extending the Block Editor

Add custom CSS class name to any core block type. See [Block Style API Reference](https://developer.wordpress.org/block-editor/reference-guides/block-api/block-styles/) for examples.

Replace `console.log()` part in the `myguten.js` file with:

```JavaScript
wp.blocks.registerBlockStyle( 'core/quote', {
    name: 'fancy-quote',
    label: 'Fancy Quote',
} );
```

This fragment uses `registerBlockStyle` function from `wp.blocks` package, so you must specify this package as a dependency when you enqueue the script. Add `array( 'wp-blocks' )` to the `myguten-plugin.php` file.

See [Package Reference](https://developer.wordpress.org/block-editor/reference-guides/packages/) for list of available packages and what objects they export.

Add a new post in the editor and add a quote block. Now there is new Fancy Quote style available for selection.

Registered block style adds CSS class name in a form `is-style-<name>`. Fancy Quote style gets `is-style-fancy-quote`.

Create `style.css` file with:

```CSS
.is-style-fancy-quote {
	color: tomato;
}
```

Enqueue the CSS file in the `myguten-plugin.php` using `enqueue_block_assets` hook:

```PHP
function myguten_stylesheet() {
    wp_enqueue_style( 'myguten-style', plugins_url( 'style.css', __FILE__ ) );
}
add_action( 'enqueue_block_assets', 'myguten_stylesheet' );
```

Reload post in the editor and you should notice color change.

## Troubleshooting

Use your browser's console and debugger to check for errors and loaded assets.

Use `console.log()` and `debugger` statements to set some checkpoints in your code when troubleshooting.

Find some information in [Mozilla Web Console Documentation](https://firefox-source-docs.mozilla.org/devtools-user/web_console/index.html) and [MDN's `debugger` statement](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/debugger).

## Scope Your Code

Write your code inside functions to scope variables locally and to not [override each other's variables](https://developer.wordpress.org/block-editor/how-to-guides/javascript/scope-your-code/).

Use anonymous functions to scope your code. [Immediately-Invoked Function Expression](https://developer.mozilla.org/en-US/docs/Glossary/IIFE) is the most popular way of invoking such a function. The pattern is:

```JavaScript
( function () {
    // your code goes here
} )();
```

You can use a variable from the global scope in a such way:

```JavaScript
( function ( parameter ) {
    console.log( parameter );
} )( window.globalScopeVariable );
```

You do not have to scope your code if you use ES6 modules for your `*.js` files and your code will run only in modern browsers (though some of them do not support [modules](https://caniuse.com/#feat=es6-module)).

## JavaScript Build Setup

To use ESNext and JSX syntax in your plugin projects you ave to setup development environment which allows transformation of these to code that browsers can understand.

WordPress uses Webpack and Babel under the hood but simplify installation and configuration in a form of a [@wordpress/scripts](https://www.npmjs.com/package/@wordpress/scripts) package.

See the [@wordpress/scripts package documentation](https://developer.wordpress.org/block-editor/packages/packages-scripts/) on configuration details. It should be sufficient to go on with defaults.

See the [Gutenberg Examples repository](https://github.com/wordpress/gutenberg-examples/) for example plugins. Directories with `-esnext` suffix show ESNext and JSX syntax examples.

Setup you development environment as described in [these instructions](../01-development-environment/README-01-development-environment.md).

Create test block directory:

```shell
mkdir myguten-block
cd myguten-block

npm init
```

Install development packages with `npm`:

```shell
npm install --save-dev --save-exact @wordpress/scripts
```

The scripts package expects te source file to be found at `src/index.js`, and will save compiled output to `build/index.js`.

Enqueue `build/index.js` in the admin screen as described earlier.

Add development and build mode scripts in plugin's `package.json` file in the `scripts` section.

Create `.gitignore` file and add `node_modules` and `build` directories in it. These directories are build by scripts and not needed in the source.

Use created `index.asset.php` file (`wp-scripts` ver 5.0.0+) to automatically set dependency list and version in plugin's main PHP file:

```PHP
$asset_file = include( plugin_dir_path( __FILE__ ) . 'build/index.asset.php');
 
wp_register_script(
    'myguten-block',
    plugins_url( 'build/index.js', __FILE__ ),
    $asset_file['dependencies'],
    $asset_file['version']
);
```

Standard development workflow is:

1. Install dependencies: `npm install`.
2. Start development builds: `npm start`.
3. Develop. Test. Repeat.
4. Create production build: `npm run build`.

## ESNext Syntax

ESNext is the latest JavaScript standard.

Most common ESNext syntax patterns used throughout the Gutenberg project are:

1. [Destructuring assignments](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment).
2. [Arrow function expressions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions).
3. [Import statements](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import).
