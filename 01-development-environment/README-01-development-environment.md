# Development Environment

Recommended setup for the Block Editor development ([link](https://developer.wordpress.org/block-editor/getting-started/devenv/)).

1. Install Node development tools

For Mac and Linux download and install [Node Version Manager](https://github.com/nvm-sh/nvm#installing-and-updating) (nvm).
For Windows download and run [Node.js installer](https://nodejs.org/en/download/).

Install and use the highest supported release number of Node.js with _Maintenance LTS_ status.
```shell
nvm install <release-number>
```

2. WordPress Development Site

Present recommendation is to download, install, and start [Docker Desktop](https://www.docker.com/products/docker-desktop) following the instructions for your OS.
If you prefer free and open source setup use [Docker Engine](https://docs.docker.com/engine/install/) instructions.

Install WordPress environment tool.
```shell
npm -g install @wordpress/env
```
Start the environment from an existing plugin or theme directory, or a new working directory.
```shell
wp-env start
```

You will have a full WordPress site installed, navigate to: http://localhost:8888/ using your browser, log in to the WordPress dashboard at http://localhost:8888/wp-admin/ using Username “admin” and Password “password”, without the quotes. For more information controlling the Docker environment see the @wordpress/env package [readme](https://developer.wordpress.org/block-editor/reference-guide/packages/packages-env/).

Instead of Docker you may want to use local bare metal server install or MAMP/WAMP/LAMP bundled environments (AMP solution stacks).

3. Code Editor

You can use any text editor to write code. Choose one that has a good support for JavaScript and React, since most of the Block Editor customization is made in this programming language and library. If you intend to write WordPress themes or generally websites and web applications look for editors and plugins that support PHP (classic themes and plugins), HTML & CSS (Emmet, linters and preprocessors) and JSON schema completion (modern themes).
