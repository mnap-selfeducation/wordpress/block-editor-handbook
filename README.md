# Block Editor Handbook

Private notes and copy-pasting from the WordPress' [Block Editor Handbook](https://developer.wordpress.org/block-editor/).

WordPress' block editor tutorials and how-to guides include basic setup, blocks building, full site editing and reference guides excerpts.

## Getting Started

Links and lists of sources, texts and tutorials of the handbook: https://developer.wordpress.org/block-editor/getting-started/.
Use this link as a starting point.

Getting started tutorials:

[Development Environment](./01-development-environment/README-01-development-environment.md)
[Create a Block Tutorial](./02-create-a-block-tutorial/README-02-create-a-block-tutorial.md)
[Full Site Editing](./03-full-site-editing/README-03-full-site-editing.md)

## How-to Guides

[Getting Started with JavaScript](./howto-getting-started-with-javascript/README-howto-getting-started-with-javascript.md)
[Development Platform](./howto-development-platform/README-howto-development-platform.md)
