# Development Platform

WordPress development platform is also a set of JavaScript tools and packages available in the [WordPress npm repository](https://www.npmjs.com/org/wordpress).

## UI Components

The [WordPress Components package](https://developer.wordpress.org/block-editor/designers-developers/developers/components/) contains a set of UI components you can use in your project. See the [WordPress Storybook](https://wordpress.github.io/gutenberg/) site for an interactive guide to the available components and settings.

To use components in your project install it as a dependency:

```shell
npm install --save @wordpress/components
```

Example usage in React:

```JavaScript
import { Button } from '@wordpress/components';

function MyApp() {
	return <Button>Hello Button</Button>;
}
```

You should link directly or copy `node_modules/@wordpress/components/build-style/style.css` components styles for them to appear correctly.

## Development Scripts

The [wp-scripts package](https://developer.wordpress.org/block-editor/packages/packages-scripts/) is a collection of reusable scripts for JavaScript development — includes scripts for building, linting, and testing — all with no additional configuration files.

To use scripts as your development tools install package as a development dependency:

```shell
npm install --save-dev @wordpress/scripts
```

Example `package.json` scripts:

```JSON
{
    "scripts": {
        "build": "wp-scripts build",
        "check-engines": "wp-scripts check-engines",
        "check-licenses": "wp-scripts check-licenses",
        "format": "wp-scripts format",
        "lint:css": "wp-scripts lint-style",
        "lint:js": "wp-scripts lint-js",
        "lint:md:docs": "wp-scripts lint-md-docs",
        "lint:pkg-json": "wp-scripts lint-pkg-json",
        "packages-update": "wp-scripts packages-update",
        "plugin-zip": "wp-scripts plugin-zip",
        "start": "wp-scripts start",
        "test:e2e": "wp-scripts test-e2e",
        "test:unit": "wp-scripts test-unit-js"
    }
}
```

## Block Editor

The [`@wordpress/block-editor` package](https://developer.wordpress.org/block-editor/packages/packages-block-editor/) allows you to create and use standalone block editors.
